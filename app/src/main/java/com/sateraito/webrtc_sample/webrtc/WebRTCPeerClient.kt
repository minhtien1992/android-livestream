package com.sateraito.webrtc_sample.webrtc

import android.app.Application
import android.content.Context
import android.util.Log
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import org.json.JSONObject
import org.webrtc.*


class WebRTCPeerClient(
    context: Application,
    observer: PeerConnection.Observer
) {

    companion object {
        private const val LOCAL_TRACK_ID = "local_track"
        private const val LOCAL_STREAM_ID = "local_track"
    }

    var myClientId: String = ""
    var remoteClientId: String = ""

    private val rootEglBase: EglBase = EglBase.create()

    private var localAudioTrack : AudioTrack? = null
    private var localVideoTrack : VideoTrack? = null
    val TAG = "WebRTCPeerClient"

    var remoteSessionDescription : SessionDescription? = null

    val db = Firebase.database

    init {
        initPeerConnectionFactory(context)
    }

    private val iceServer = listOf(
        PeerConnection.IceServer.builder("stun:stun.l.google.com:19302")
            .createIceServer()
    )

    private val peerConnectionFactory by lazy { buildPeerConnectionFactory() }
    private val videoCapturer by lazy { getVideoCapturer(context) }
    private var isCapturing: Boolean = false

    private val audioSource by lazy { peerConnectionFactory.createAudioSource(MediaConstraints())}
    private val localVideoSource by lazy { peerConnectionFactory.createVideoSource(false) }
    private val peerConnection by lazy { buildPeerConnection(observer) }

    private fun initPeerConnectionFactory(context: Application) {
        val options = PeerConnectionFactory.InitializationOptions.builder(context)
            .setEnableInternalTracer(true)
            .setFieldTrials("WebRTC-H264HighProfile/Enabled/")
            .createInitializationOptions()
        PeerConnectionFactory.initialize(options)
    }

    private fun buildPeerConnectionFactory(): PeerConnectionFactory {
        return PeerConnectionFactory
            .builder()
            .setVideoDecoderFactory(DefaultVideoDecoderFactory(rootEglBase.eglBaseContext))
            .setVideoEncoderFactory(DefaultVideoEncoderFactory(rootEglBase.eglBaseContext, true, true))
            .setOptions(PeerConnectionFactory.Options().apply {
                disableEncryption = false
                disableNetworkMonitor = true
            })
            .createPeerConnectionFactory()
    }

    private fun buildPeerConnection(observer: PeerConnection.Observer) = peerConnectionFactory.createPeerConnection(
        iceServer,
        observer
    )

    private fun getVideoCapturer(context: Context) =
        Camera2Enumerator(context).run {
            deviceNames.find {
                isFrontFacing(it)
            }?.let {
                createCapturer(it, null)
            } ?: throw IllegalStateException()
        }

    fun initSurfaceView(view: SurfaceViewRenderer) = view.run {
        setMirror(true)
        setEnableHardwareScaler(true)
        init(rootEglBase.eglBaseContext, null)
    }

    fun startLocalVideoCapture(localVideoOutput: SurfaceViewRenderer) {
        if (!isCapturing) {
            isCapturing = true
            val surfaceTextureHelper = SurfaceTextureHelper.create(Thread.currentThread().name, rootEglBase.eglBaseContext)
            (videoCapturer as VideoCapturer).initialize(surfaceTextureHelper, localVideoOutput.context, localVideoSource.capturerObserver)
            videoCapturer.startCapture(320, 240, 60)
            Log.d(TAG, "video capturer: ${videoCapturer}")
            localAudioTrack = peerConnectionFactory.createAudioTrack(LOCAL_TRACK_ID + "_audio", audioSource);
            localVideoTrack = peerConnectionFactory.createVideoTrack(LOCAL_TRACK_ID, localVideoSource)
            localVideoTrack?.addSink(localVideoOutput)
            val localStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID)
            localStream.addTrack(localVideoTrack)
            localStream.addTrack(localAudioTrack)
            peerConnection?.addStream(localStream)
        }
    }

    fun sendMessage(data: JSONObject, dst: String) {
        val messageRef = db.getReference("messages/${dst}")
        messageRef.push().setValue(data.toString())
    }

    private fun PeerConnection.call(sdpObserver: SdpObserver, dst: String) {
        val constraints = MediaConstraints().apply {
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"))
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
            optional.add(MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"))
        }

        createOffer(object : SdpObserver by sdpObserver {
            override fun onCreateSuccess(sessionDescription: SessionDescription?) {
                setLocalDescription(object : SdpObserver {
                    override fun onSetFailure(p0: String?) {}
                    override fun onSetSuccess() {
                        val message = JSONObject()
                        message.put("type", "offer")
                        message.put("src", myClientId)
                        message.put("sdp", sessionDescription!!.description)
                        sendMessage(message, dst)
                        Log.d(TAG, "send offer")
                    }
                    override fun onCreateSuccess(p0: SessionDescription?) {}
                    override fun onCreateFailure(p0: String?) {}
                }, sessionDescription)
                sdpObserver.onCreateSuccess(sessionDescription)
            }

            override fun onSetFailure(p0: String?) {}
            override fun onCreateFailure(p0: String?) {}
        }, constraints)
    }

    private fun PeerConnection.answer(sdpObserver: SdpObserver, dst: String) {
        val constraints = MediaConstraints().apply {
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"))
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
        }
        createAnswer(object : SdpObserver by sdpObserver {
            override fun onCreateSuccess(sessionDescription: SessionDescription?) {
                val message = JSONObject()
                message.put("type", "answer")
                message.put("src", myClientId)
                message.put("sdp", sessionDescription!!.description)
                sendMessage(message, dst)
                Log.d(TAG, "send answer")
                setLocalDescription(object : SdpObserver {
                    override fun onSetFailure(p0: String?) {}
                    override fun onSetSuccess() {
                        Log.d(TAG, "set local description success")
                    }
                    override fun onCreateSuccess(p0: SessionDescription?) {}
                    override fun onCreateFailure(p0: String?) {}
                }, sessionDescription)
                sdpObserver.onCreateSuccess(sessionDescription)
            }

            override fun onCreateFailure(p0: String?) {
                Log.d(TAG, "answer failed: ${p0}")
            }
        }, constraints)
    }

    fun call(sdpObserver: SdpObserver, dst: String) = peerConnection?.call(sdpObserver, dst)

    fun answer(sdpObserver: SdpObserver, dst: String) = peerConnection?.answer(sdpObserver, dst)

    fun onRemoteSessionReceived(sessionDescription: SessionDescription) {
        remoteSessionDescription = sessionDescription
        peerConnection?.setRemoteDescription(object : SdpObserver {
            override fun onSetFailure(p0: String?) {
                Log.d(TAG, "set remote description failed ${p0}")
            }
            override fun onSetSuccess() {
                Log.d(TAG, "set remote description success")
            }
            override fun onCreateSuccess(p0: SessionDescription?) {}
            override fun onCreateFailure(p0: String?) {}
        }, sessionDescription)
    }

    fun sendIceCandidate(iceCandidate: IceCandidate?) {
        val message = JSONObject()
        message.put("type", "candidate")
        message.put("src", myClientId)
        val candidate = JSONObject()
        candidate.put("sdpMid", iceCandidate?.sdpMid)
        candidate.put("sdpMLineIndex", iceCandidate?.sdpMLineIndex)
        candidate.put("sdpCandidate", iceCandidate?.sdp)
        message.put("candidate", candidate)
        sendMessage(message, remoteClientId)
    }

    fun addIceCandidate(iceCandidate: IceCandidate?) {
        Log.d(TAG, "iceCandidate: ${iceCandidate}")
        peerConnection?.addIceCandidate(iceCandidate)
    }

    fun endCall(dst: String) {
        val message = JSONObject()
        message.put("type", "endCall")
        message.put("src", myClientId)
        sendMessage(message, dst)
        peerConnection?.close()
        videoCapturer?.stopCapture()
        localVideoSource.dispose()
        localVideoTrack?.dispose()
        localAudioTrack?.dispose()
    }

    fun close() {
        peerConnection?.close()
    }

    fun enableVideo(videoEnabled: Boolean) {
        if (localVideoTrack !=null)
            localVideoTrack?.setEnabled(videoEnabled)
    }

    fun enableAudio(audioEnabled: Boolean) {
        if (localAudioTrack != null)
            localAudioTrack?.setEnabled(audioEnabled)
    }
    fun switchCamera() {
        videoCapturer.switchCamera(null)
    }
}