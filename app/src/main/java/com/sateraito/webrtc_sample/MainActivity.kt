package com.sateraito.webrtc_sample

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*


import com.neovisionaries.ws.client.WebSocket
import com.neovisionaries.ws.client.WebSocketAdapter
import com.neovisionaries.ws.client.WebSocketFactory
import org.json.JSONObject
//import javax.swing.text.html.HTML.Tag.U




//kurento
//import org.kurento.client.KurentoClient
//import org.kurento.client.PlayerEndpoint
//import org.kurento.client.RecorderEndpoint
//import org.kurento.client.WebRtcEndpoint
//import org.webrtc.AudioTrack
//import org.webrtc.MediaStream
//import org.webrtc.PeerConnection
//import org.webrtc.PeerConnectionFactory
//import org.webrtc.VideoTrack


class MainActivity : AppCompatActivity() {
    private lateinit var ws: WebSocket
//    private val peer: PeerConnection? = null
////    private val factory: PeerConnectionFactory? = null
////
////    private val localAudioTrack: AudioTrack? = null
////    private val localVideoTrack: VideoTrack? = null
////    private val localMediaStream: MediaStream? = null
////
////    private val kurentoClient: KurentoClient? = null
////    private val webRTCEndpoint: WebRtcEndpoint? = null
////    private val playerEndpoint: PlayerEndpoint? = null
////    private val recorderEndpoint: RecorderEndpoint? = null

    companion object {
        private const val CAMERA_AUDIO_PERMISSION_REQUEST_CODE = 1
        private const val CAMERA_PERMISSION = Manifest.permission.CAMERA
        private const val AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO
    }

    val TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        save.setOnClickListener {
            if (device_id.text.toString().trim().isNullOrEmpty())
                device_id.error = "カメラIDが必須です。"
            else if (device_name.text.toString().trim().isNullOrEmpty())
                device_name.error = "カメラ名が必要です。"
            else {
                startStream()
            }
        }
        checkCameraAndAudioPermission()
        val wsFactory = WebSocketFactory()
        ws = wsFactory.createSocket("wss://webrtc-dapp2.sateraito.jp:8443/record")
        ws.addListener(object : WebSocketAdapter() {
            override fun  onConnected(ws: WebSocket, headers: Map<String, List<String>>) {
                Log.d("MY_APP", "Connected")
                val message = JSONObject()
                message.put("id", "start")
                message.put("device_id", 122)
                message.put("device_name", 122)
                message.put("sdpOffer", "122")
                message.put("google_id", 122)
                ws.sendText(message.toString())
            }
            override fun onTextMessage(ws: WebSocket, text: String) {
                Log.d("MY_APP", "Message received: $text")
                val data = JSONObject(text)
                Log.d("MY_APP", "Message received: $data")
                when (text) {
                    "restart" -> {

                    }
                    "startResponse" -> {
                    }

                    "error" -> {
                    }

                    "iceCandidate" -> {
                    }

                    "origin_startResponse" -> {

                    }

                    "origin_iceCandidate" -> {

                    }

                    "mask_setting" -> {
                    }

                    "changeFile" -> {
                    }

                    "checkId" -> {
                    }
                }

            }
        })
        ws.connectAsynchronously()
    }

    private fun startStream(){

    }

    private fun checkCameraAndAudioPermission() {
        if ((ContextCompat.checkSelfPermission(this, MainActivity.CAMERA_PERMISSION)
                    != PackageManager.PERMISSION_GRANTED) &&
            (ContextCompat.checkSelfPermission(this, MainActivity.AUDIO_PERMISSION)
                    != PackageManager.PERMISSION_GRANTED)) {
            requestCameraAndAudioPermission()
        } else {
            onCameraAndAudioPermissionGranted()
        }
    }

    private fun onCameraAndAudioPermissionGranted() {
    }

    private fun onCameraPermissionDenied() {
        Toast.makeText(this, "カメラと音声の権限が拒否されました！", Toast.LENGTH_LONG).show()
    }

    private fun requestCameraAndAudioPermission(dialogShown: Boolean = false) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                MainActivity.CAMERA_PERMISSION
            ) &&
            ActivityCompat.shouldShowRequestPermissionRationale(this, MainActivity.AUDIO_PERMISSION) &&
            !dialogShown) {
            showPermissionRationaleDialog()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(
                MainActivity.CAMERA_PERMISSION,
                MainActivity.AUDIO_PERMISSION
            ), MainActivity.CAMERA_AUDIO_PERMISSION_REQUEST_CODE
            )
        }
    }

    private fun showPermissionRationaleDialog() {
        AlertDialog.Builder(this)
            .setTitle("確認")
            .setMessage("カメラと音声の権限が必須です。")
            .setPositiveButton("許可") { dialog, _ ->
                dialog.dismiss()
                requestCameraAndAudioPermission(true)
            }
            .setNegativeButton("拒否") { dialog, _ ->
                dialog.dismiss()
                onCameraPermissionDenied()
            }
            .show()
    }

    override fun onStop() {
        super.onStop()
    }
}