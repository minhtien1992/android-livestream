package com.sateraito.webrtc_sample.firebase

import android.annotation.SuppressLint
import android.util.Log
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import org.webrtc.IceCandidate
import org.webrtc.SessionDescription

class RealtimeDBClient (
    private val myClientID : String,
    private val listener: RealtimeDBEventListener
) {
    val db = Firebase.database
    val TAG = "RealtimeDBClient"
    private lateinit var messageListener: ChildEventListener

    init {
        Log.d(TAG, "handleMessages")
        messageListener = object: ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val message = snapshot.value as Map<String, Any>
                if (message != null) {
                    val messageType = message.get("type")
                    when (messageType) {
                        "requestCall" -> {
                            val src = message.get("src").toString()
                            Log.d(TAG, "${src} requested call")
                            listener.onRequestCall(src)
                        }
                        "acceptCall" -> {
                            val src = message.get("src").toString()
                            listener.onAcceptCall(src)
                        }
                        "offer" -> {
                            Log.d(TAG, "offer: ${message}")
                            val src = message.get("src").toString()
                            val sdp = message.get("sdp").toString()
                            val description = SessionDescription(SessionDescription.Type.OFFER, sdp)
                            listener.onOfferReceived(src, description)
                        }
                        "answer" -> {
                            val src = message.get("src").toString()
                            val sdp = message.get("sdp").toString()
                            val description = SessionDescription(SessionDescription.Type.ANSWER, sdp)
                            listener.onAnswerReceived(src, description)
                        }
                        "candidate" -> {
                            val src = message.get("src").toString()
                            val candidateData = message.get("candidate") as Map<String, Any>
                            val sdpMid = candidateData.get("sdpMid").toString()
                            val sdpMLineIndex = candidateData.get("sdpMLineIndex").toString().toInt()
                            val sdpCandidate = candidateData.get("candidate").toString()
                            val candidate = IceCandidate(sdpMid, sdpMLineIndex, sdpCandidate)
                            listener.onIceCandidateReceived(src, candidate)
                        }
                        "endCall" -> {
                            listener.onCallEnded()
                        }
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildRemoved(snapshot: DataSnapshot) {}
        }
        handleMessages()
    }

    private fun handleMessages() {
        db.getReference("messages/${myClientID}").addChildEventListener(messageListener)
    }

    fun removeAll() {
        db.getReference("messages").setValue(null)
        db.getReference("users").setValue(null)
    }

    fun destroy() {
        db.getReference("messages/${myClientID}").removeEventListener(messageListener)
    }
}