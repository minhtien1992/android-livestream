package com.sateraito.webrtc_sample.firebase

import org.webrtc.IceCandidate
import org.webrtc.SessionDescription

interface RealtimeDBEventListener {
    fun onRequestCall(src: String)
    fun onAcceptCall(src: String)
    fun onOfferReceived(src: String, description: SessionDescription)
    fun onAnswerReceived(src: String, description: SessionDescription)
    fun onIceCandidateReceived(src: String, iceCandidate: IceCandidate)
    fun onCallEnded()
}