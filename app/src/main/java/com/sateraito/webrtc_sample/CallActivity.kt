package com.sateraito.webrtc_sample

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import com.sateraito.webrtc_sample.firebase.RealtimeDBClient
import com.sateraito.webrtc_sample.firebase.RealtimeDBEventListener
import com.sateraito.webrtc_sample.webrtc.PeerAudioManager
import com.sateraito.webrtc_sample.webrtc.PeerConnectionObserver
import com.sateraito.webrtc_sample.webrtc.WebRTCPeerClient
import com.sateraito.webrtc_sample.webrtc.WebRTCSdpObserver
import kotlinx.android.synthetic.main.activity_call.*
import org.json.JSONObject
import org.webrtc.*

class CallActivity : AppCompatActivity() {

    companion object {
        private const val CAMERA_AUDIO_PERMISSION_REQUEST_CODE = 1
        private const val CAMERA_PERMISSION = Manifest.permission.CAMERA
        private const val AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO
    }

    private lateinit var peerClient: WebRTCPeerClient
    private lateinit var rtdbClient: RealtimeDBClient

    private val audioManager by lazy { PeerAudioManager.create(this) }

    val TAG = "CallActivity"

    private var myClientId = ""
    private var remoteClientId = ""
    private var isMute = false
    private var isVideoPaused = false
    private var inSpeakerMode = true
    private var startedCall = false

    private val sdpObserver = object : WebRTCSdpObserver() {
        override fun onCreateSuccess(p0: SessionDescription?) {
            super.onCreateSuccess(p0)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call)

        if (intent.hasExtra("myClientId"))
            myClientId = intent.getStringExtra("myClientId")!!
        if (intent.hasExtra("remoteClientId"))
            remoteClientId = intent.getStringExtra("remoteClientId")!!

        audioManager.selectAudioDevice(PeerAudioManager.AudioDevice.SPEAKER_PHONE)
        switch_camera_button.setOnClickListener {
            peerClient.switchCamera()
        }

        audio_output_button.setOnClickListener {
            if (inSpeakerMode) {
                inSpeakerMode = false
                audio_output_button.setImageResource(R.drawable.ic_baseline_hearing_24)
                audioManager.setDefaultAudioDevice(PeerAudioManager.AudioDevice.EARPIECE)
            } else {
                inSpeakerMode = true
                audio_output_button.setImageResource(R.drawable.ic_baseline_speaker_up_24)
                audioManager.setDefaultAudioDevice(PeerAudioManager.AudioDevice.SPEAKER_PHONE)
            }
        }
        video_button.setOnClickListener {
            if (isVideoPaused) {
                isVideoPaused = false
                video_button.setImageResource(R.drawable.ic_baseline_videocam_off_24)
            } else {
                isVideoPaused = true
                video_button.setImageResource(R.drawable.ic_baseline_videocam_24)
            }
            peerClient.enableVideo(isVideoPaused)
        }
        mic_button.setOnClickListener {
            if (isMute) {
                isMute = false
                mic_button.setImageResource(R.drawable.ic_baseline_mic_off_24)
            } else {
                isMute = true
                mic_button.setImageResource(R.drawable.ic_baseline_mic_24)
            }
            peerClient.enableAudio(isMute)
        }
        end_call_button.setOnClickListener {
            peerClient.endCall(remoteClientId)
            finish()
            startActivity(Intent(this@CallActivity, MainActivity::class.java))
        }
        rtdbClient =  RealtimeDBClient(myClientId, rtdbEventListener())
        peerClient = WebRTCPeerClient(
            application,
            object : PeerConnectionObserver() {
                override fun onIceCandidate(p0: IceCandidate?) {
                    super.onIceCandidate(p0)
                    Log.e(TAG, "onIceCandidate")
                    peerClient.sendIceCandidate(p0)
                }

                override fun onAddStream(p0: MediaStream?) {
                    super.onAddStream(p0)
                    Log.e(TAG, "onAddStream")
                    p0?.videoTracks?.get(0)?.addSink(remote_view)
                }
            }
        )
        peerClient.myClientId = myClientId
        peerClient.remoteClientId = remoteClientId
        peerClient.initSurfaceView(remote_view)
        peerClient.initSurfaceView(local_view)
        if (intent.hasExtra("onRequestCall")) {
            peerClient.startLocalVideoCapture(local_view)
            val message = JSONObject()
            message.put("type", "acceptCall")
            message.put("src", myClientId)
            peerClient.sendMessage(message, remoteClientId)
        } else {
            val message = JSONObject()
            message.put("type", "requestCall")
            message.put("src", myClientId)
            peerClient.sendMessage(message, remoteClientId)
        }
    }

    private fun rtdbEventListener() = object : RealtimeDBEventListener {
        override fun onRequestCall(src: String) {}
        override fun onAcceptCall(src: String) {
            startCall()
        }
        override fun onOfferReceived(src: String, description: SessionDescription) {
            peerClient.onRemoteSessionReceived(description)
            peerClient.answer(sdpObserver, remoteClientId)
            remote_view_loading.isGone = true
        }

        override fun onAnswerReceived(src: String, description: SessionDescription) {
            peerClient.onRemoteSessionReceived(description)
            remote_view_loading.isGone = true
        }

        override fun onIceCandidateReceived(src: String, iceCandidate: IceCandidate) {
            peerClient.addIceCandidate(iceCandidate)
        }

        override fun onCallEnded() {
            peerClient.close()
            rtdbClient.removeAll()
            finish()
            startActivity(Intent(this@CallActivity, MainActivity::class.java))
        }
    }

    private fun startCall() {
        if (!startedCall) {
            startedCall = true
            peerClient.startLocalVideoCapture(local_view)
            peerClient.call(sdpObserver,remoteClientId)
        }
    }

    override fun onDestroy() {
        rtdbClient.destroy()
        local_view.release()
        remote_view.release()
        super.onDestroy()
    }
}